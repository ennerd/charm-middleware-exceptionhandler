charm/exception-middleware
==========================

A simple middleware implementation that catches exceptions thrown later in the
exception stack and returns a PSR-7 Response with the error description.

By configuring the `error_handler`, you can customize error pages for public 
users, and fall back to the built-in error page for developers.

Configuration
-------------

```
<?php
use Charm\Middleware\ExceptionMiddleware;

$middleware = new ExceptionMiddleware([
    'error_handler' => function(\Throwable $e) {
        if ($e->getCode() === 404) {
            return render_page_not_found();
        }
        // If error handler returns null, the ExceptionMiddleware will render a response.
        return null;
    }
]);
```
